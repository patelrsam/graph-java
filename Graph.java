package graphs;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentSkipListSet;

import com.sun.glass.ui.MenuItem.Callback;
import com.sun.org.apache.xpath.internal.operations.Bool;

/**
 * Implements a graph. We use two maps: one map for adjacency properties 
 * (adjancencyMap) and one map (dataMap) to keep track of the data associated 
 * with a vertex. 
 * 
 * @author cmsc132
 * 
 * @param <E>
 */
public class Graph<E> {
	/* You must use the following maps in your implementation */
	private HashMap<String, HashMap<String, Integer>> adjacencyMap;
	private HashMap<String, E> dataMap;
	private ArrayList<Boolean> tagList;
	
	
	public Graph(){
		adjacencyMap = new HashMap<>();
		dataMap = new HashMap<>();
		tagList = new ArrayList<Boolean>();
		
	}

	public void addDirectedEdge(String startVertexName, String endVertexName, int cost){
		HashMap<String, Integer> added = new HashMap<>();
		
		if(adjacencyMap.get(startVertexName)==null){
			added.put(endVertexName, cost);
			adjacencyMap.put(startVertexName, added);
		}else{
			added = adjacencyMap.get(startVertexName);
			added.put(endVertexName, cost);
		}
	}
	
	public void addVertex(String vertexName, E data) {
		if (dataMap.get(vertexName) == null) {
			dataMap.put(vertexName, data);
		} else {
			throw new IllegalArgumentException();
		}
	}

	public Map<String, Integer> getAdjacentVertices(String vertexName){
		Map<String,Integer> map = new HashMap<String, Integer>();
		map.putAll(adjacencyMap.get(vertexName));
		return map;
	}
	
	public int getCost(String startVertexName, String endVertexName) {
		HashMap<String, Integer> map;
		try {
			map = adjacencyMap.get(startVertexName);
			return map.get(endVertexName);

		} catch (NullPointerException e) {
			throw new IllegalArgumentException();
		}
	}

	public Set<String> getVertices ( ){
		Set<String> set = new HashSet<String>();
		
		for(Entry<String,E> e:dataMap.entrySet()){
			set.add(e.getKey());
		}
		return set;
	}
	@Override
	public String toString() {
		String s = "";
		String[] array = dataMap.keySet().toArray(new String[dataMap.size()]);
		
		quickSort(array, 0, array.length-1);
		s+="Vertices: ";
		s += Arrays.toString(array) + "\n";
		s += "Edges:\n";

		for(int i =0;i<array.length-1;i++){
			if (adjacencyMap.get(array[i]) == null) {
				s += "Vertex(" + array[i] + ")" + "--->" + "{}" + "\n";
			} else {
				s += "Vertex(" + array[i] + ")" + "--->"
						+ adjacencyMap.get(array[i]).toString() + "\n";
			}
		}
		
		s += "Vertex(" + array[array.length-1] + ")" + "--->"
				+ adjacencyMap.get(array[array.length-1]).toString();
		
		return s;

	}
	
	private void quickSort(String[] arrayIN, int lower, int upper) {
		if (arrayIN == null || arrayIN.length == 0) {
			return;
		}
		int l = lower;
		int u = upper;
		String pivot = arrayIN[lower + (upper - lower) / 2];
		
		while (l <= u) {
			while (arrayIN[l].compareTo(pivot) < 0) {
				l++;
			}
			while (arrayIN[u].compareTo(pivot) > 0) {
				u--;
			}
			if (l <= u) {
				swap(arrayIN, l, u);
				l++;
				u--;
			}
		}

		if (lower < u) {
			quickSort(arrayIN, lower, u);
		}

		if (upper > l) {
			quickSort(arrayIN, l, upper);
		}
	}
	
	private void swap(String[]arrayIN,int x,int y){
		String temp=arrayIN[x];
		arrayIN[x]=arrayIN[y];
		arrayIN[y]=temp;
	}
	 
	
	public E getData (String vertex){
		return dataMap.get(vertex);
	}
	
	public void doDepthFirstSearch (String startVertexName, CallBack< E > callback){
		Stack<String> stack = new Stack<>();
		HashSet<String> visited = new HashSet<>();
		
		
		stack.push(startVertexName);
		
		while(!stack.isEmpty()){
			String vertex = stack.pop();
			
			if(!stack.contains(vertex)){
				callback.processVertex(vertex, dataMap.get(vertex));
				visited.add(vertex);
			}
			
			for(Entry<String, Integer> adjacency:adjacencyMap.get(vertex).entrySet()){
				if(!visited.contains(adjacency.getKey())){
					stack.push(adjacency.getKey());
				}
			}
		}
		
		
		
	}

	public void doBreadthFirstSearch (String startVertexName, CallBack< E > callback){		
		Queue<String> stack = new PriorityQueue<String>();
		HashSet<String> visited = new HashSet<>();
		
		
		stack.add(startVertexName);
		
		while(!stack.isEmpty()){
			String vertex = stack.poll();
			
			if(!stack.contains(vertex)){
				callback.processVertex(vertex, dataMap.get(vertex));
				visited.add(vertex);
			}
			
			for(Entry<String, Integer> adjacency:adjacencyMap.get(vertex).entrySet()){
				if(!visited.contains(adjacency.getKey())){
					stack.add(adjacency.getKey());
				}
			}
		}
	}
	
	public int doDijkstras(String startVertexName, String endVertexName,ArrayList<String> shortestPath) {
		
		if(!dataMap.containsKey(startVertexName)||!dataMap.containsKey(endVertexName)){
			throw new IllegalArgumentException();
		}
			
		if(adjacencyMap.get(startVertexName)==null){
			shortestPath.add("None");
			return -1;
		}
		if(startVertexName.equals(endVertexName)){
			shortestPath.add(startVertexName);
			return 0;
		}
		shortestPath.clear();	
		HashMap<String, Vertex> cost = new HashMap<>();// cost: sets a cost of the each node from the start path
		TreeSet<Vertex> fringe = new TreeSet<>();//visited
	
		
		for (Entry<String, E> a : dataMap.entrySet()) {
			
			cost.put(a.getKey(), new Vertex(a.getKey(),Integer.MAX_VALUE,null));//setting vertices value to "infinity" 
			//fringe.add(cost.get(a.getKey()));
		}

		//cost.put(startVertexName, 0);//setting all the 
		cost.put(startVertexName, new Vertex(startVertexName, 0, null));
		fringe.add(cost.get(startVertexName));
		
		// this is where the main computation happens 
		while (!fringe.isEmpty()) {
			Vertex ent = fringe.pollFirst();//gets the minimum

			String lastMinObj = ent.getName();//name of the minimum 
			for(String e:adjacencyMap.get(lastMinObj).keySet()){
				int curCost = cost.get(e).getCost();
				int altCost = this.getAdjacentVertices(lastMinObj).get(e)+cost.get(lastMinObj).getCost();
				
				if(altCost< curCost){
					cost.get(e).setCost(altCost);
					cost.get(e).setPrvious(lastMinObj);
					fringe.add(cost.get(e));
					
				}
			}
		}

		String cur=endVertexName;
		//adding to the shortest path arraylist
		while(cur!=null){
//			if(cost.get(cur).getCost()==Integer.MAX_VALUE){
//				shortestPath.add("None");
//				cost.get(cur).setCost(-1);
//				break;
//			}
			shortestPath.add(cur);
			cur=cost.get(cur).getPrevious();
		}
		
		Collections.reverse(shortestPath);
		
		for(String s:shortestPath){
			if(cost.get(s).getCost()==Integer.MAX_VALUE){
				shortestPath.clear();
				shortestPath.add("None");
				return -1;
			}
		}
		
		return cost.get(endVertexName).getCost();
	}
	
	//vestex holds the name of the vertex, its previous shortest path and the cost from the start vertex to the current vertex 
	private class Vertex implements Comparable<Vertex>{
		private String ver,prv;
		private Integer val;
		public Vertex(String ver, Integer value,String prv){
			this.ver=ver;
			val=value;
			this.prv=prv;
		}
		////////////////////////////////////////
		
		        //Getters and Setters//
		
		///////////////////////////////////////
		public void setCost (int cost){
			val=cost;
		}
		
		public void setPrvious(String previous){
			prv=previous;
		}
		
		public String getName(){
			return ver;
		}
		public Integer getCost(){
			return val;
		}
		
		public String getPrevious(){
			return prv;
		}

		// methods that tree sets need
		@Override
		public int compareTo(Vertex o) {
			return val.compareTo(o.getCost());
		}
		
		@Override
		public boolean equals(Object obj) {
			if(obj==null){
				return false;
			}
			
			if(obj.getClass()!=getClass()){
				return false;
			}
			
			Vertex object = (Vertex) obj;
			
			if(ver.equals(object.ver)){
				return true;
			}
			return false;
		}
		
		@Override
		public int hashCode() {
			return ver.hashCode();
		}
		
	}
	
	
	

}